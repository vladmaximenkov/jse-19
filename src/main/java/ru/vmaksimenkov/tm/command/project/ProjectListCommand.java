package ru.vmaksimenkov.tm.command.project;

import ru.vmaksimenkov.tm.model.Project;

public final class ProjectListCommand extends AbstractProjectCommand {

    @Override
    public String arg() {
        return null;
    }

    @Override
    public String name() {
        return "project-list";
    }

    @Override
    public String description() {
        return "Show project list";
    }

    @Override
    public void execute() {
        final String userId = serviceLocator.getAuthService().getUserId();
        System.out.println("[PROJECT LIST]");
        System.out.println(
                String.format("\t| %-36s | %-12s | %-20s | %-30s | %-30s | %-30s |", "ID", "STATUS", "NAME", "CREATED", "STARTED", "FINISHED")
        );
        int index = 1;
        for (final Project project : serviceLocator.getProjectService().findAll(userId)) {
            System.out.println(index + ".\t" + project);
            index++;
        }
    }

}
