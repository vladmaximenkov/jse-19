package ru.vmaksimenkov.tm.service;

import ru.vmaksimenkov.tm.api.repository.IProjectRepository;
import ru.vmaksimenkov.tm.api.repository.ITaskRepository;
import ru.vmaksimenkov.tm.api.service.IProjectTaskService;
import ru.vmaksimenkov.tm.exception.empty.EmptyIdException;
import ru.vmaksimenkov.tm.exception.empty.EmptyNameException;
import ru.vmaksimenkov.tm.exception.entity.ProjectNotFoundException;
import ru.vmaksimenkov.tm.exception.entity.TaskNotFoundException;
import ru.vmaksimenkov.tm.exception.system.IndexIncorrectException;
import ru.vmaksimenkov.tm.model.Task;

import java.util.List;

import static ru.vmaksimenkov.tm.util.ValidationUtil.checkIndex;
import static ru.vmaksimenkov.tm.util.ValidationUtil.isEmpty;

public class ProjectTaskService implements IProjectTaskService {

    final private ITaskRepository taskRepository;

    final private IProjectRepository projectRepository;

    public ProjectTaskService(final IProjectRepository projectRepository, final ITaskRepository taskRepository) {
        this.projectRepository = projectRepository;
        this.taskRepository = taskRepository;
    }

    @Override
    public List<Task> findAllTaskByProjectId(final String userId, final String projectId) {
        if (isEmpty(projectId)) throw new EmptyIdException();
        if (!taskRepository.existsByProjectId(userId, projectId)) throw new TaskNotFoundException();
        return taskRepository.findAllByProjectId(userId, projectId);
    }

    @Override
    public Task bindTaskByProjectId(final String userId, final String projectId, final String taskId) {
        if (isEmpty(projectId) || isEmpty(taskId)) throw new EmptyIdException();
        if (!projectRepository.existsById(userId, projectId)) throw new ProjectNotFoundException();
        return taskRepository.bindTaskPyProjectId(userId, projectId, taskId);
    }

    @Override
    public Task unbindTaskFromProject(final String userId, final String taskId) {
        if (isEmpty(taskId)) throw new EmptyIdException();
        return taskRepository.unbindTaskFromProject(userId, taskId);
    }

    @Override
    public void removeProjectById(final String userId, final String projectId) {
        if (isEmpty(projectId)) throw new EmptyIdException();
        if (!projectRepository.existsById(userId, projectId)) throw new ProjectNotFoundException();
        taskRepository.removeAllByProjectId(userId, projectId);
        projectRepository.removeById(userId, projectId);
    }

    @Override
    public void removeProjectByName(final String userId, final String projectName) {
        if (isEmpty(projectName)) throw new EmptyNameException();
        String projectId = projectRepository.getIdByName(userId, projectName);
        if (isEmpty(projectId)) throw new EmptyIdException();
        taskRepository.removeAllByProjectId(userId, projectId);
        projectRepository.removeOneByName(userId, projectName);
    }

    @Override
    public void removeProjectByIndex(final String userId, final Integer projectIndex) {
        if (!checkIndex(projectIndex, projectRepository.size(userId))) throw new IndexIncorrectException();
        String projectId = projectRepository.getIdByIndex(userId, projectIndex);
        if (isEmpty(projectId)) throw new EmptyIdException();
        taskRepository.removeAllByProjectId(userId, projectId);
        projectRepository.removeOneByIndex(userId, projectIndex);
    }

    @Override
    public void clearTasks(final String userId) {
        taskRepository.removeAllBinded(userId);
        projectRepository.clear(userId);
    }

}
