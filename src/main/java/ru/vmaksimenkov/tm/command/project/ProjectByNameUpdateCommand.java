package ru.vmaksimenkov.tm.command.project;

import ru.vmaksimenkov.tm.exception.empty.EmptyNameException;
import ru.vmaksimenkov.tm.exception.entity.ProjectNotFoundException;
import ru.vmaksimenkov.tm.util.TerminalUtil;

import static ru.vmaksimenkov.tm.util.ValidationUtil.isEmpty;

public final class ProjectByNameUpdateCommand extends AbstractProjectCommand {

    @Override
    public String arg() {
        return null;
    }

    @Override
    public String name() {
        return "project-update-by-name";
    }

    @Override
    public String description() {
        return "Update project by name";
    }

    @Override
    public void execute() {
        final String userId = serviceLocator.getAuthService().getUserId();
        System.out.println("[UPDATE PROJECT]");
        System.out.println("[ENTER NAME:]");
        final String name = TerminalUtil.nextLine();
        if (!serviceLocator.getProjectService().existsByName(userId, name)) throw new ProjectNotFoundException();
        System.out.println("ENTER NEW NAME:");
        final String nameNew = TerminalUtil.nextLine();
        if (isEmpty(nameNew)) throw new EmptyNameException();
        System.out.println("ENTER DESCRIPTION:");
        serviceLocator.getProjectService().updateProjectByName(userId, name, nameNew, TerminalUtil.nextLine());
    }

}
