package ru.vmaksimenkov.tm.command.task;

import ru.vmaksimenkov.tm.model.Task;
import ru.vmaksimenkov.tm.util.TerminalUtil;

import java.util.List;

public final class TaskByProjectIdListCommand extends AbstractTaskCommand {

    @Override
    public String arg() {
        return null;
    }

    @Override
    public String name() {
        return "task-list-by-project-id";
    }

    @Override
    public String description() {
        return "Show task list by project id";
    }

    @Override
    public void execute() {
        final String userId = serviceLocator.getAuthService().getUserId();
        System.out.println("[TASK LIST BY PROJECT ID]");
        System.out.println("ENTER PROJECT ID:");
        final List<Task> taskList = serviceLocator.getProjectTaskService().findAllTaskByProjectId(userId, TerminalUtil.nextLine());
        System.out.println(
                String.format("\t| %-36s | %-12s | %-20s | %-30s | %-30s | %-30s | %s |", "ID", "STATUS", "NAME", "CREATED", "STARTED", "FINISHED", "PROJECT")
        );
        int index = 1;
        for (final Task task : taskList) {
            System.out.println(index + ".\t" + task);
            index++;
        }
    }

}
