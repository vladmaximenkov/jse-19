package ru.vmaksimenkov.tm.api.repository;

import ru.vmaksimenkov.tm.api.IRepository;
import ru.vmaksimenkov.tm.model.Task;

import java.util.List;

public interface ITaskRepository extends IRepository<Task> {

    List<Task> findAllByProjectId(String userId, String projectId);

    Task bindTaskPyProjectId(String userId, String projectId, String taskId);

    Task unbindTaskFromProject(String userId, String taskId);

    Task findOneByIndex(String userId, Integer index);

    Task findOneByName(String userId, String name);

    String getIdByIndex(String userId, Integer index);

    boolean existsByProjectId(String userId, String projectId);

    boolean existsByName(String userId, String name);

    void removeOneByName(String userId, String name);

    void removeOneByIndex(String userId, Integer index);

    void removeAllBinded(String userId);

    void removeAllByProjectId(String userId, String projectId);

}
