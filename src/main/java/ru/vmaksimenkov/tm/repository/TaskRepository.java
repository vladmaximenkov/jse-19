package ru.vmaksimenkov.tm.repository;

import ru.vmaksimenkov.tm.api.repository.ITaskRepository;
import ru.vmaksimenkov.tm.exception.entity.TaskNotFoundException;
import ru.vmaksimenkov.tm.model.Task;

import java.util.ArrayList;
import java.util.List;

import static ru.vmaksimenkov.tm.util.ValidationUtil.isEmpty;

public class TaskRepository extends AbstractRepository<Task> implements ITaskRepository {

    @Override
    public List<Task> findAllByProjectId(final String userId, final String projectId) {
        List<Task> taskList = new ArrayList<>();
        for (final Task task : list) {
            if (projectId.equals(task.getProjectId()) && userId.equals(task.getUserId())) taskList.add(task);
        }
        return taskList;
    }

    @Override
    public Task findOneByIndex(final String userId, final Integer index) {
        int i = 0;
        for (final Task task : list) {
            if (userId.equals(task.getUserId())) i++;
            if (index.equals(i)) return task;
        }
        throw new TaskNotFoundException();
    }

    @Override
    public Task findOneByName(final String userId, final String name) {
        for (final Task task : list) {
            if (name.equals(task.getName()) && userId.equals(task.getUserId())) return task;
        }
        throw new TaskNotFoundException();
    }

    @Override
    public Task bindTaskPyProjectId(final String userId, final String projectId, final String taskId) {
        final Task task = findById(userId, taskId);
        task.setProjectId(projectId);
        return task;
    }

    @Override
    public Task unbindTaskFromProject(final String userId, final String taskId) {
        final Task task = findById(userId, taskId);
        task.setProjectId(null);
        return task;
    }

    @Override
    public String getIdByIndex(final String userId, final Integer index) {
        int i = 0;
        for (final Task task : list) {
            if (userId.equals(task.getUserId())) i++;
            if (index.equals(i)) return task.getId();
        }
        throw new TaskNotFoundException();
    }

    @Override
    public boolean existsByProjectId(final String userId, final String projectId) {
        for (final Task task : list) {
            if (projectId.equals(task.getProjectId()) && userId.equals(task.getUserId())) return true;
        }
        return false;
    }

    @Override
    public boolean existsByName(final String userId, final String name) {
        for (final Task task : list) {
            if (name.equals(task.getName()) && userId.equals(task.getUserId())) return true;
        }
        return false;
    }

    @Override
    public void removeAllByProjectId(final String userId, final String projectId) {
        for (int i = list.size(); i-- > 0; ) {
            if (projectId.equals(list.get(i).getProjectId()) && userId.equals(list.get(i).getUserId())) {
                list.remove(i);
            }
        }
    }

    @Override
    public void removeAllBinded(final String userId) {
        for (int i = list.size(); i-- > 0; ) {
            if (!isEmpty(list.get(i).getProjectId()) && userId.equals(list.get(i).getUserId())) {
                list.remove(i);
            }
        }
    }

    @Override
    public void removeOneByIndex(final String userId, final Integer index) {
        remove(findOneByIndex(userId, index));
    }

    @Override
    public void removeOneByName(final String userId, final String name) {
        remove(findOneByName(userId, name));
    }

}
