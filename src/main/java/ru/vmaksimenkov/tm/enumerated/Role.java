package ru.vmaksimenkov.tm.enumerated;

import ru.vmaksimenkov.tm.exception.entity.RoleNotFoundException;

import static ru.vmaksimenkov.tm.util.ValidationUtil.checkRole;

public enum Role {

    USER("User"),
    ADMIN("Administrator");

    private final String displayName;

    Role(String displayName) {
        this.displayName = displayName;
    }

    public static Role getRole(String s) {
        s = s.toUpperCase();
        if (!checkRole(s)) throw new RoleNotFoundException();
        return valueOf(s);
    }

    public String getDisplayName() {
        return displayName;
    }

}
