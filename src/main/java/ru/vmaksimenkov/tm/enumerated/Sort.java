package ru.vmaksimenkov.tm.enumerated;

import ru.vmaksimenkov.tm.comparator.ComparatorByCreated;
import ru.vmaksimenkov.tm.comparator.ComparatorByName;
import ru.vmaksimenkov.tm.comparator.ComparatorByStarted;
import ru.vmaksimenkov.tm.comparator.ComparatorByStatus;
import ru.vmaksimenkov.tm.exception.entity.SortNotFoundException;

import java.util.Comparator;

import static ru.vmaksimenkov.tm.util.ValidationUtil.checkSort;

public enum Sort {

    NAME("Sort by name", ComparatorByName.getInstance()),
    CREATED("Sort by created", ComparatorByCreated.getInstance()),
    STARTED("Sort by date start", ComparatorByStarted.getInstance()),
    STATUS("Sort by status", ComparatorByStatus.getInstance());

    private final String displayName;

    private final Comparator comparator;

    Sort(String displayName, Comparator comparator) {
        this.displayName = displayName;
        this.comparator = comparator;
    }

    public static Sort getSort(String s) {
        s = s.toUpperCase();
        if (!checkSort(s)) throw new SortNotFoundException();
        return valueOf(s);
    }

    public Comparator getComparator() {
        return comparator;
    }

    public String getDisplayName() {
        return displayName;
    }

}
