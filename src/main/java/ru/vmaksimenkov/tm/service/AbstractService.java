package ru.vmaksimenkov.tm.service;

import ru.vmaksimenkov.tm.api.IRepository;
import ru.vmaksimenkov.tm.api.IService;
import ru.vmaksimenkov.tm.exception.empty.EmptyIdException;
import ru.vmaksimenkov.tm.exception.entity.ComparatorNotFoundException;
import ru.vmaksimenkov.tm.exception.entity.ProjectNotFoundException;
import ru.vmaksimenkov.tm.model.AbstractEntity;

import java.util.Comparator;
import java.util.List;

import static ru.vmaksimenkov.tm.util.ValidationUtil.isEmpty;

public abstract class AbstractService<E extends AbstractEntity> implements IService<E> {

    private final IRepository<E> repository;

    public AbstractService(IRepository<E> repository) {
        this.repository = repository;
    }

    @Override
    public List<E> findAll(final String userId) {
        return repository.findAll(userId);
    }

    @Override
    public List<E> findAll(final String userId, final Comparator<E> comparator) {
        if (comparator == null) throw new ComparatorNotFoundException();
        return repository.findAll(userId, comparator);
    }

    @Override
    public E findById(final String userId, final String id) {
        if (isEmpty(id)) throw new EmptyIdException();
        return repository.findById(userId, id);
    }

    @Override
    public int size(final String userId) {
        if (repository.size() == 0) return 0;
        return repository.size(userId);
    }

    @Override
    public int size() {
        return repository.size();
    }

    @Override
    public boolean existsById(final String userId, final String id) {
        if (isEmpty(id)) throw new EmptyIdException();
        return repository.existsById(userId, id);
    }

    @Override
    public void clear(final String userId) {
        repository.clear(userId);
    }

    @Override
    public void removeById(final String userId, final String id) {
        if (isEmpty(id)) throw new EmptyIdException();
        repository.removeById(userId, id);
    }


    @Override
    public void add(final E e) {
        if (e == null) throw new ProjectNotFoundException();
        repository.add(e);
    }

    @Override
    public void remove(final E e) {
        if (e == null) throw new ProjectNotFoundException();
        repository.remove(e);
    }

}
