package ru.vmaksimenkov.tm.util;

import ru.vmaksimenkov.tm.enumerated.Role;
import ru.vmaksimenkov.tm.enumerated.Sort;
import ru.vmaksimenkov.tm.enumerated.Status;

public interface ValidationUtil {

    static boolean isEmpty(final String value) {
        return value == null || value.isEmpty();
    }

    static boolean checkIndex(final int index, int size) {
        if (index < 1) return false;
        return index <= size;
    }

    static boolean checkStatus(final String status) {
        for (Status s : Status.values()) {
            if (s.name().equals(status)) {
                return true;
            }
        }
        return false;
    }

    static boolean checkSort(final String sortOrder) {
        for (Sort s : Sort.values()) {
            if (s.name().equals(sortOrder)) {
                return true;
            }
        }
        return false;
    }

    static boolean checkRole(final String role) {
        for (Role s : Role.values()) {
            if (s.name().equals(role)) {
                return true;
            }
        }
        return false;
    }

}
