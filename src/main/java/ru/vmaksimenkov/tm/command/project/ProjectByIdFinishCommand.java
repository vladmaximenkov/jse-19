package ru.vmaksimenkov.tm.command.project;

import ru.vmaksimenkov.tm.exception.entity.NoProjectsException;
import ru.vmaksimenkov.tm.util.TerminalUtil;

public final class ProjectByIdFinishCommand extends AbstractProjectCommand {

    @Override
    public String arg() {
        return null;
    }

    @Override
    public String name() {
        return "project-finish-by-id";
    }

    @Override
    public String description() {
        return "Finish project by id";
    }

    @Override
    public void execute() {
        final String userId = serviceLocator.getAuthService().getUserId();
        if (serviceLocator.getProjectService().size(userId) < 1) throw new NoProjectsException();
        System.out.println("[FINISH PROJECT]");
        System.out.println("ENTER ID:");
        serviceLocator.getProjectService().finishProjectById(userId, TerminalUtil.nextLine());
    }

}
