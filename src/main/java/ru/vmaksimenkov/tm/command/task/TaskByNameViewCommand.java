package ru.vmaksimenkov.tm.command.task;

import ru.vmaksimenkov.tm.util.TerminalUtil;

public final class TaskByNameViewCommand extends AbstractTaskCommand {

    @Override
    public String arg() {
        return null;
    }

    @Override
    public String name() {
        return "task-view-by-name";
    }

    @Override
    public String description() {
        return "View task by name";
    }

    @Override
    public void execute() {
        final String userId = serviceLocator.getAuthService().getUserId();
        System.out.println("[SHOW TASK]");
        System.out.println("ENTER NAME:");
        showTask(serviceLocator.getTaskService().findOneByName(userId, TerminalUtil.nextLine()));
    }

}
