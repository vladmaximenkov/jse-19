package ru.vmaksimenkov.tm.repository;

import ru.vmaksimenkov.tm.api.IRepository;
import ru.vmaksimenkov.tm.exception.entity.EntityNotFoundException;
import ru.vmaksimenkov.tm.model.AbstractEntity;

import java.util.ArrayList;
import java.util.Comparator;
import java.util.List;

public abstract class AbstractRepository<E extends AbstractEntity> implements IRepository<E> {

    protected final List<E> list = new ArrayList<>();

    @Override
    public List<E> findAll(final String userId) {
        List<E> entities = new ArrayList<>();
        for (final E entity : list) {
            if (userId.equals(entity.getUserId())) entities.add(entity);
        }
        return entities;
    }

    @Override
    public List<E> findAll(final String userId, final Comparator<E> comparator) {
        final List<E> entities = new ArrayList<>();
        for (E e : list) if (userId.equals(e.getUserId())) entities.add(e);
        entities.sort(comparator);
        return entities;
    }

    @Override
    public E findById(final String userId, final String id) {
        for (final E e : list) {
            if (id.equals(e.getId()) && userId.equals(e.getUserId())) return e;
        }
        throw new EntityNotFoundException();
    }

    @Override
    public int size() {
        return list.size();
    }

    @Override
    public int size(String userId) {
        int i = 0;
        for (final E e : list) {
            if (userId.equals(e.getUserId())) i++;
        }
        return i;
    }

    @Override
    public boolean existsById(final String userId, final String id) {
        for (final E e : list) {
            if (id.equals(e.getId()) && userId.equals(e.getUserId())) return true;
        }
        return false;
    }

    @Override
    public void clear(final String userId) {
        for (int i = list.size(); i-- > 0; ) {
            if (userId.equals(list.get(i).getUserId())) {
                list.remove(i);
            }
        }
    }

    @Override
    public void removeById(final String userId, final String id) {
        remove(findById(userId, id));
    }

    @Override
    public void add(E e) {
        list.add(e);
    }

    @Override
    public void remove(final E e) {
        list.remove(e);
    }

}
