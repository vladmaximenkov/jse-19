package ru.vmaksimenkov.tm.command.task;

import ru.vmaksimenkov.tm.enumerated.Status;
import ru.vmaksimenkov.tm.exception.system.IndexIncorrectException;
import ru.vmaksimenkov.tm.util.TerminalUtil;

import java.util.Arrays;

import static ru.vmaksimenkov.tm.util.ValidationUtil.checkIndex;

public final class TaskByIndexSetStatusCommand extends AbstractTaskCommand {

    @Override
    public String arg() {
        return null;
    }

    @Override
    public String name() {
        return "task-set-status-by-index";
    }

    @Override
    public String description() {
        return "Set task status by index";
    }

    @Override
    public void execute() {
        final String userId = serviceLocator.getAuthService().getUserId();
        System.out.println("[SET TASK STATUS]");
        System.out.println("ENTER INDEX:");
        final int index = TerminalUtil.nextNumber();
        if (!checkIndex(index, serviceLocator.getTaskService().size(userId))) throw new IndexIncorrectException();
        System.out.println("ENTER STATUS:");
        System.out.println(Arrays.toString(Status.values()));
        serviceLocator.getTaskService().setTaskStatusByIndex(userId, index, Status.getStatus(TerminalUtil.nextLine()));

    }

}
