package ru.vmaksimenkov.tm.api.service;

import ru.vmaksimenkov.tm.model.Task;

import java.util.List;

public interface IProjectTaskService {

    List<Task> findAllTaskByProjectId(String userId, String projectId);

    Task bindTaskByProjectId(String userId, String projectId, String taskId);

    Task unbindTaskFromProject(String userId, String taskId);

    void removeProjectById(String userId, String projectId);

    void removeProjectByName(String userId, String projectName);

    void removeProjectByIndex(String userId, Integer projectIndex);

    void clearTasks(String userId);

}
