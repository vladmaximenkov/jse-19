package ru.vmaksimenkov.tm.command.user;

import ru.vmaksimenkov.tm.exception.entity.UserNotFoundException;
import ru.vmaksimenkov.tm.exception.user.AlreadyLoggedInException;
import ru.vmaksimenkov.tm.util.TerminalUtil;

import static ru.vmaksimenkov.tm.util.TerminalUtil.nextLine;

public final class UserLoginCommand extends AbstractUserCommand {

    @Override
    public String arg() {
        return null;
    }

    @Override
    public String name() {
        return "user-login";
    }

    @Override
    public String description() {
        return "Log in to the system";
    }

    @Override
    public void execute() {
        if (serviceLocator.getAuthService().isAuth()) throw new AlreadyLoggedInException();
        System.out.println("[LOGIN]");
        System.out.println("ENTER LOGIN:");
        final String login = nextLine();
        if (!serviceLocator.getUserService().existsByLogin(login)) throw new UserNotFoundException();
        System.out.println("ENTER PASSWORD:");
        serviceLocator.getAuthService().login(login, TerminalUtil.nextLine());
    }

}
