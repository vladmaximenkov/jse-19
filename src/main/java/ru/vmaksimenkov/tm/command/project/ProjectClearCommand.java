package ru.vmaksimenkov.tm.command.project;

public final class ProjectClearCommand extends AbstractProjectCommand {

    @Override
    public String arg() {
        return null;
    }

    @Override
    public String name() {
        return "project-clear";
    }

    @Override
    public String description() {
        return "Delete all projects";
    }

    @Override
    public void execute() {
        final String userId = serviceLocator.getAuthService().getUserId();
        System.out.println("[PROJECT CLEAR]");
        serviceLocator.getProjectTaskService().clearTasks(userId);
    }

}
