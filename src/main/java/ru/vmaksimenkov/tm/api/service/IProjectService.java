package ru.vmaksimenkov.tm.api.service;

import ru.vmaksimenkov.tm.api.IService;
import ru.vmaksimenkov.tm.enumerated.Status;
import ru.vmaksimenkov.tm.model.Project;

public interface IProjectService extends IService<Project> {

    Project add(String userId, String name, String description);

    Project findOneByIndex(String userId, Integer index);

    Project findOneByName(String userId, String name);

    Project updateProjectById(String userId, String id, String name, String description);

    Project updateProjectByName(String userId, String name, String nameNew, String description);

    Project updateProjectByIndex(String userId, Integer index, String name, String description);

    Project startProjectById(String userId, String id);

    Project startProjectByName(String userId, String name);

    Project startProjectByIndex(String userId, Integer index);

    Project finishProjectById(String userId, String id);

    Project finishProjectByName(String userId, String name);

    Project finishProjectByIndex(String userId, Integer index);

    Project setProjectStatusById(String userId, String id, Status status);

    Project setProjectStatusByName(String userId, String name, Status status);

    Project setProjectStatusByIndex(String userId, Integer index, Status status);

    boolean existsByName(String userId, String name);

}
