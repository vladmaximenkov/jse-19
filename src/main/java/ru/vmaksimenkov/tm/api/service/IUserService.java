package ru.vmaksimenkov.tm.api.service;

import ru.vmaksimenkov.tm.api.IService;
import ru.vmaksimenkov.tm.enumerated.Role;
import ru.vmaksimenkov.tm.model.User;

public interface IUserService extends IService<User> {

    User create(String login, String password);

    User create(String login, String password, String email);

    User create(String login, String password, Role role);

    User findByLogin(String login);

    User findById(String id);

    User setRole(String userId, Role role);

    User updateUser(String userId, String firstName, String lastName, String middleName);

    boolean existsByLogin(String login);

    boolean existsByEmail(String email);

    void removeByLogin(String userId, String login);

    void setPassword(String userId, String password);

}
